package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (\n" +
            "\tidPizza INTEGER,\n" +
            "\tidIngredient INTEGER,\n" +
            "\tPRIMARY KEY(idPizza,idIngredient),\n" +
            "\tFOREIGN KEY(idPizza) REFERENCES Pizzas(id),\n" +
            "\tFOREIGN KEY(idIngredient) REFERENCES Ingredient(id));")
    void createAssociationTable();

    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropPizzaTable();

    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();

    @SqlUpdate("INSERT INTO Pizzas(name) VALUES(:name)")
    @GetGeneratedKeys
    long insertPizzaTable(String name);

    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation(idPizza, idIngredient) VALUES(:idPizza, :idIngredient)")
    void insertAssociationTable(long idPizza, long idIngredient);

    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(long id);

    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(String name);

    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void remove(long id);

    @SqlQuery("SELECT * FROM PizzaIngredientsAssociation WHERE idPizza = :id")
    @RegisterBeanMapper(Pizza.class)
    List<Ingredient> getIngredients(long id);


    @Transaction
    default void createTableAndIngredientAssociation() {
        createAssociationTable();
        createPizzaTable();
    }
}
