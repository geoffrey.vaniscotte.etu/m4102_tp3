package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import java.util.List;
import java.util.Objects;

public class Pizza {
    private long id;
    private String name;
    private List<Ingredient> ingredients;

    public Pizza() {
    }

    public Pizza(long id, String name, List<Ingredient> ingredients) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static PizzaDto toDto(Pizza i) {
        PizzaDto dto = new PizzaDto();
        dto.setId(i.getId());
        dto.setName(i.getName());
        dto.setIngredients(i.getIngredients());
        return dto;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public static Pizza fromDto(PizzaDto dto) {
        Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());
        pizza.setIngredients(dto.getIngredients());
        return pizza;
    }

    public static PizzaCreateDto toCreateDto(Pizza pizza) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
        dto.setIngredients(pizza.getIngredients());
        return dto;
    }

    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
        Pizza pizza = new Pizza();
        pizza.setName(dto.getName());
        pizza.setIngredients(dto.getIngredients());
        return pizza;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return id == pizza.id &&
                Objects.equals(name, pizza.name) &&
                Objects.equals(ingredients, pizza.ingredients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, ingredients);
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ingredients=" + ingredients +
                '}';
    }
}
