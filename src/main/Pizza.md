# Pizza API

| Opération | URI               | Action réalisé                            | Retour                                              |
|-----------|-------------------|-------------------------------------------|-----------------------------------------------------|
| GET       | /pizzas           | récupère l'ensemble des pizzas            | 200 et un tableau de pizzas                         |
| GET       | /pizzas/{id}      | Récupère la pizza d'identifiant id        | 200 et une pizza                                    |
|           |                   |                                           | 404 si id est inconnu                               |
| GET       | /pizzas/{id}/name | Récupère le nom de la pizza               | 200 et le nom de la pizza                           |
|           |                   |                                           | 404 si id est inconnu                               |
| POST      | /pizzas           | Création d'une pizza                      | 201 et l'URI de la ressource créée + représentation |
|           |                   |                                           | 400 si les informations ne sont pas correctes       |
|           |                   |                                           | 409 si la pizza éxiste déjà                         |
| DELETE    | /pizzas/{id}      | destruction de la pizza d'indentifiant id | 204 si l'opération réussi                           |
|           |                   |                                           | 404 si id est inconnu                               |


Une pizza comporte uniquement un identifiant, un nom et une liste d'ingrédients.
Sa représentation JSON prendra donc la forme suivante :
```JSON {
  "id": 1,
  "name": "3 fromages",
  "ingrédients":[
     {
        "id": 1,
        "name": "reblochon"
      },
      {
        "id": 2,
        "name": "chèvre"
      }
  ]
}
```